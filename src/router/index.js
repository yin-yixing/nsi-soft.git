//引入vue-router
import { createRouter, createWebHashHistory} from 'vue-router'

//导入组件
import Nsisoft from '@/components/Nsisoft.vue'
import Nav from '@/components/Nav.vue'
import TeaTeam from '@/components/TeaTeam/TeaTeam.vue'
import Soft from '@/components/Major/Soft.vue'
import Accrued from '@/components/Major/Accrued.vue'
import Mobile from '@/components/Major/Mobile.vue'
import BigData from '@/components/Major/BigData.vue'
import Cloud from '@/components/Major/Cloud.vue'

import ScLife from '@/components/NsiSchool/ScLife.vue'
import ScFG from '@/components/NsiSchool/ScFG.vue'

import Dean from '@/components/School/Dean.vue'
import Nsinews from '@/components/Nsinews/Nsinews.vue'
import Leader from '@/components/School/Leader.vue'

import Plan from '@/components/RecruitStu/Plan.vue'
import Publicity from '@/components/RecruitStu/Publicity.vue'
//创建vue-routers实例，定义实例的选项
const router = createRouter({
    history: createWebHashHistory(),
    routes:[
        {
            path:'/',
            component:Nav
            
        },
        {
            path:'/Nav',
            component:Nav
            
        },
        {
            path:'/nsisoft',
            component:Nsisoft
            
        },
        {
            path:'/teaTeam',
            component:TeaTeam
            
        },
        {
            path:'/soft',
            component:Soft
            
        },
        {
            path:'/cloud',
            component:Cloud
            
        },
        {
            path:'/accrued',
            component:Accrued
            
        },
        {
            path:'/mobile',
            component:Mobile
            
        },
        {
            path:'/bigdata',
            component:BigData
            
        },
        {
            path:'/sclife',
            component:ScLife
            
        },
        {
            path:'/scfg',
            component:ScFG
            
        },
        {
            path:'/dean',
            component:Dean
            
        },
        {
            path:'/leader',
            component:Leader
            
        },
        {
            path:'/nsinews',
            component:Nsinews
            
        },
        {
            path:'/plan',
            component:Plan
            
        },
        {
            path:'/publicity',
            component:Publicity
            
        },
    
    ]
})

//到导出路由
export default router